package com.hiepnv.solidexample

import com.google.firebase.auth.FirebaseAuth
import java.io.File

class MainRepository(
    private val auth: FirebaseAuth,
    private val fileLogger: CustomErrorFileLogger
) {
    fun loginUser(email: String, password: String) {
        try {
            auth.signInWithEmailAndPassword(email, password)
        } catch (e: Exception) {
            fileLogger.logError(e.message.toString())
        }
    }
}

open class FileLogger {
    open fun logError(error: String) {
        val file = File("errors.txt")
        file.appendText(
            text = error
        )
    }
}

class CustomErrorFileLogger : FileLogger() {
    fun customLogError(error: String) {
        val file = File("custom_errors.txt")
        file.appendText(
            text = error
        )
    }
}

open class Duck() {
    open fun swim() {
        // Real duck swim
    }

    open fun quack() {
        // Real duck quack logic
    }
}

class ToyDuck : Duck() {
    private var hasNoBatteries: Boolean = true

    override fun swim() {
        if (hasNoBatteries) {
            return
        }
        // Toy duck swim logic
    }

    override fun quack() {
        if (hasNoBatteries) {
            return
        }
        // Toy duck quack logic
    }
}

interface Shape {
    fun draw()
}

open class Rectangle : Shape {
    open var width: Float = 0f
    open var height: Float = 0f

    override fun draw() {
        println("This is rectangle with width $width and height $height")
    }
}

class Square : Rectangle() {
    override var width: Float = 0f
        set(value) {
            field = value
            height = value
        }

    override var height: Float = 0f
        set(value) {
            field = value
            width = value
        }

    override fun draw() {
        println("This is square with side $width")
    }
}


    class AndroidDeveloper {
        fun developMobileApp() {
            println("Developing Android Application by using Kotlin")
        }
    }

    class IosDeveloper {
        fun developMobileApp() {
            println("Developing iOS Application by using Swift")
        }
    }

    fun main() {
        val androidDeveloper = AndroidDeveloper()
        val iosDeveloper = IosDeveloper()

        androidDeveloper.developMobileApp()
        iosDeveloper.developMobileApp()
    }




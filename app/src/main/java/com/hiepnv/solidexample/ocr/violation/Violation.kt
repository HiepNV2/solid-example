package com.hiepnv.solidexample.ocr.violation

data class Rectangle(val width: Double, val length: Double)

data class Circle(val radius: Double)

class AreaFactory {
    fun calculateArea(shapes: ArrayList<Any>): Double {
        var area: Double = 0.toDouble()
        for (shape in shapes) {
            area += when (shape) {
                is Rectangle -> shape.width * shape.length
                is Circle -> shape.radius * shape.radius * Math.PI
                else -> throw RuntimeException("Shape not supported")
            }
        }
        return area
    }
}
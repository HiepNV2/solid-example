package com.hiepnv.solidexample.ocr

class GoodCode {

    open class Robot {
        open fun work() {}
    }

    class Cutter : Robot() {
        override fun work() {
            println("Now, I can cut & paint")
        }
    }

    class Painter : Robot() {
        override fun work() {
            println("Now, I can paint")
        }
    }

    class Work {
        fun working(robot: Robot) {
            robot.work()
        }
    }

}



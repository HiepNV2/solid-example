package com.hiepnv.solidexample.ocr.solution

interface Shape {
    fun getArea(): Double
}

data class Rectangle(val width: Double, val height: Double) : Shape {
    override fun getArea() = width * height
}

data class Circle(val radius: Double) : Shape {
    override fun getArea() = radius * radius * Math.PI
}

// New class
data class Triangle(val height: Double, val base: Double) : Shape {
    override fun getArea() = (height * base) / 2
}

class AreaFactory {
    fun calculateArea(shapes: ArrayList<Shape>): Double {
        var area: Double = 0.toDouble()
        for (shape in shapes) {
            area += shape.getArea()
        }
        return area
    }
}
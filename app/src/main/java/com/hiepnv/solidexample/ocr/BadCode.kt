package com.hiepnv.solidexample.ocr

class BadCode {

    open class Robot {}

    class Cutter : Robot() {
        fun cut() {}
    }

    class Painter : Robot() {
        fun paint() {}
    }

    class Work {
        fun working(robot: Robot) {
            if (robot is Cutter) println("I can´t cut")
            else if (robot is Painter) println("Now, I can paint")
        }
    }

}



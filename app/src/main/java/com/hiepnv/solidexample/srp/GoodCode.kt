package com.hiepnv.solidexample.srp

class GoodCode {

    open class Robot {
        class Chef : Robot() {
            fun cook() {}
        }

        class Gardener : Robot() {
            fun plant() {}
        }

        class Painter : Robot() {
            fun paint() {}
        }

        class Driver : Robot() {
            fun drive() {}
        }
    }

}


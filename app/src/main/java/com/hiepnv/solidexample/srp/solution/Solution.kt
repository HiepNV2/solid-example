package com.hiepnv.solidexample.srp.solution

data class User(
    val id: Int,
    val name: String,
    val password: String
)

class AuthenticationService {
    fun signIn() {
        // This method will do signing in operations
    }

    fun signOut() {
        // This method will do signing out operations
    }
}
package com.hiepnv.solidexample.dip.solution

class Solution {

    interface CalculatorOperation {
        fun calculate(x: Double, y: Double): Double
    }

    class AddCalculatorOperation : CalculatorOperation {
        override fun calculate(x: Double, y: Double): Double {
            return x + y
        }
    }

    class SubtractCalculatorOperation : CalculatorOperation {
        override fun calculate(x: Double, y: Double): Double {
            return x - y
        }
    }

    class MultiplyCalculatorOperation : CalculatorOperation {
        override fun calculate(x: Double, y: Double): Double {
            return x * y
        }
    }

    class DivideCalculatorOperation : CalculatorOperation {
        override fun calculate(x: Double, y: Double): Double {
            return x / y
        }
    }

    class Calculator(private val calculatorOperation: CalculatorOperation) {
        fun solve(x: Double, y: Double): Double {
            return calculatorOperation.calculate(x, y)
        }
    }

    class CalculatorProgram {
        fun calculate(x: Double, y: Double) {
            val calculator = Calculator(AddCalculatorOperation())
        }

        fun testAdd() {
            val calculator = Calculator(AddCalculatorOperation())
            val result = calculator.solve(2.0, 1.0)
        }

        fun testSubtract() {
            val calculator = Calculator(SubtractCalculatorOperation())
            val result = calculator.solve(2.0, 1.0)
        }

        fun testMultiply() {
            val calculator = Calculator(MultiplyCalculatorOperation())
            val result = calculator.solve(2.0, 3.0)
        }

        fun testDivide() {
            val calculator = Calculator(DivideCalculatorOperation())
            val result = calculator.solve(10.0, 2.0)
        }
    }

    fun main() {
        val calculatorProgram = CalculatorProgram()
        calculatorProgram.testAdd()
    }

}
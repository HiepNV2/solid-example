package com.hiepnv.solidexample.dip

class BadCode {

    class CutterArm {
        fun cut() = println("I cut pizza with my pizza cutter arm")
    }

    class Robot {
        private val cutterArm: CutterArm = CutterArm()
        fun cutPizza() = cutterArm.cut()
    }

}

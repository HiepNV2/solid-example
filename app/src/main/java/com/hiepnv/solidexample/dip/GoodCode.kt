package com.hiepnv.solidexample.dip

class GoodCode {

    interface Tool {
        fun cutterArm()
        // Example
        // fun knifeArm()
    }

    class Robot(private val tool: Tool) {
        fun print() = println("I cut with any tool given to me")
        fun cutPizza() = tool.cutterArm()
        // Example
        // fun cutPizza() = tool.knifeArm()
    }

}

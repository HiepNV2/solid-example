package com.hiepnv.solidexample.dip.violation

class Calculator {
    fun add(x: Double, y: Double) = x + y
    fun subtract(x: Double, y: Double) = x - y
}

class CalculatorProgram {
    fun calculate(x: Double, y: Double) {
        val calculator = Calculator()
        println("add x, y = ${calculator.add(x, y)}")
        println("subtract x, y = ${calculator.subtract(x, y)}")
    }

    fun testAdd() {
        val calculator = Calculator()
        val result = calculator.add(2.0, 1.0)
    }

    fun testSubtract() {
        val calculator = Calculator()
        val result = calculator.subtract(2.0, 1.0)
    }

    fun testMultiply() {
        // ??
    }

    fun testDivide() {
        // ??
    }
}

fun main() {
    val calculatorProgram = CalculatorProgram()
    calculatorProgram.calculate(4.0, 2.0)
}



package com.hiepnv.solidexample.lsp.violation

interface Vehicle {
    fun startEngine()
    fun stopEngine()
    fun moveForward()
    fun moveBack()
}

class Car : Vehicle {
    override fun startEngine() {}
    override fun stopEngine() {}
    override fun moveForward() {}
    override fun moveBack() {}
}

class Bicycle : Vehicle {
    override fun startEngine() {
        // Bicycle don't have engine
    }
    override fun stopEngine() {
        // Bicycle don't have engine
    }
    override fun moveForward() {}
    override fun moveBack() {}
}
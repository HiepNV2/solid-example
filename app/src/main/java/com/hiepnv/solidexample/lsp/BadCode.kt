package com.hiepnv.solidexample.lsp

class BadCode {

        open class RobotSam {
            open fun serveCoffee() = println("Here´s your coffee")
        }

        // The RobotEden class is a subtype of the RobotSam class.
        class RobotEden : RobotSam() {
            override fun serveCoffee() {}
            fun serveWater() = println("I can´t make coffee but here´s water")
        }

}


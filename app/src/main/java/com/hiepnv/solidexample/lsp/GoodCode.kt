package com.hiepnv.solidexample.lsp

class GoodCode {

    open class RobotSam {
        open fun serveCoffee() = println("Here´s your coffee")
    }

    class RobotEden : RobotSam() {
        override fun serveCoffee() = println("Here´s a cappuccino")
    }

}

package com.hiepnv.solidexample.lsp.solution

interface Vehicle {
    fun moveForward()
    fun moveBack()
}

abstract class VehicleWithEngine : Vehicle {
    open fun startEngine() {}
    open fun stopEngine() {}
}

class Car : VehicleWithEngine() {
    override fun startEngine() {}
    override fun stopEngine() {}
    override fun moveForward() {}
    override fun moveBack() {}
}

class Bicycle : Vehicle {
    override fun moveForward() {}
    override fun moveBack() {}
}
package com.hiepnv.solidexample.isp.violation

interface Animal {
    fun eat()
    fun sleep()
    fun fly()
}

class Cat : Animal {
    override fun eat() {}
    override fun sleep() {}
    override fun fly() {
        // Cat can't fly
    }
}

class Bird : Animal {
    override fun eat() {}
    override fun sleep() {}
    override fun fly() {}
}
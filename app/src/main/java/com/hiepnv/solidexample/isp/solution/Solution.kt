package com.hiepnv.solidexample.isp.solution

interface Animal {
    fun eat()
    fun sleep()
}

interface FlyingAnimal {
    fun fly()
}

class Cat : Animal {
    override fun eat() {}
    override fun sleep() {}
}

class Bird : Animal, FlyingAnimal {
    override fun eat() {}
    override fun sleep() {}
    override fun fly() {}
}
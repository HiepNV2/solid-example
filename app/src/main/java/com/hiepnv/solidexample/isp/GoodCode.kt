package com.hiepnv.solidexample.isp

class GoodCode {

    interface Exercises {
        // General method
    }

    interface CanSpinAround : Exercises {
        fun spinAround()
    }

    interface CanRotateArms : Exercises {
        fun rotateArms()
    }

    interface CanWiggleAntennas : Exercises {
        fun wiggleAntennas()
    }

    class RobotWithAntennas : CanRotateArms, CanSpinAround, CanWiggleAntennas {
        override fun spinAround() {}
        override fun rotateArms() {}
        override fun wiggleAntennas() {}
        fun print() = println("Awesome!")
    }

    class RobotNotAntennas : CanRotateArms, CanSpinAround {
        override fun spinAround() {}
        override fun rotateArms() {}
        fun print() = println("Awesome!")
    }

}

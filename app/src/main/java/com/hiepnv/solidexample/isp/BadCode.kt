package com.hiepnv.solidexample.isp

class BadCode {

    interface Exercises {
        fun spinAround()
        fun rotateArms()
        fun wiggleAntennas()
    }

    class RobotWithAntennas : Exercises {
        override fun spinAround() {}
        override fun rotateArms() {}
        override fun wiggleAntennas() {}
    }

    class RobotNotAntennas : Exercises {
        override fun spinAround() {}
        override fun rotateArms() {}

        // This is not right!!!
        override fun wiggleAntennas() {
            println("Oops! But I don´t have antennas")
        }
    }

}
